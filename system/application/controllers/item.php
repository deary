<?php

class Item extends Controller {
  
	private $user_id;
	private $user_name;

	function Item()
	{
		parent::Controller();
		// Authentication
		if(! $this->session->userdata('logged_in')) { redirect('login'); }
		// User id
		$this->user_id = $this->session->userdata('user_id');
		$this->user_name = $this->session->userdata('user_name');
	}
	
	// function index()
	// {
	// 	//-- rendering
	// 	$this->load->view('shared/header');
	// 	$this->load->view('shared/menu');
	// 	$this->load->view('item/index', array());
	// 	$this->load->view('shared/footer');
	// }

	function index()
	{
		// Passphrase capture
		
		$this->load->view('shared/header');
		$this->load->view('shared/menu');
		$this->load->view('item/index');
		$this->load->view('shared/footer');
	}
	
	function create($cat_id = null)
	{

		$sql = "SELECT * FROM types";
		$query = $this->db->query($sql, array($this->user_id));
		$types = $query->result_array();
		
		$sql = "SELECT id, name FROM categories WHERE categories.user_id = ?";
		$query = $this->db->query($sql, array($this->user_id));
		$categories = $query->result_array();
		
		$data['types'] = $types;
		$data['categories'] = $categories;
						
		$this->load->view('item/create', $data);
	}
	

	
	
	function form()
	{
		$this->load->view('shared/header');
		$this->load->view('shared/menu');
		
		switch($_POST['typeid']){ 
			case 1 : $this->load->view('item/note');
			         break;
			case 2 : $this->load->view('item/link');
			         break;
			case 3 : $this->load->view('item/contact');
           		 break;
			case 4 : $this->load->view('item/event');
               break;
      		default : $this->load->view('item/note'); // Error Handling
		}

		$this->load->view('item/create');
		$this->load->view('shared/footer');
	}


	function save()
	{
		//load the model
        $this->load->model('Item_model', 'items');

        //call the insert method
        $user_id = $this->users->insertMethod($_POST);
		foreach ($_POST['category_ids'] as $c){ echo 'You selected ',$c,'<br />'; }
	}


	function view($cat_id = null, $item_id = null)
	{
		// view a specific item $item_id, 
		// or all the items under $cat_id if $item_id is null
		
		
		// list all categories and object types underneath
		// $sql = "SELECT categories.name FROM categories 
		// JOIN category_item ON categories.cat_item_id = category_item.category_id
		// JOIN items ON category_item.item_id = items.cat_item_id
		// GROUP BY categories.name";
		
		$sql = "SELECT categories.name, categories.id FROM categories, category_item, items 
		WHERE categories.cat_item_id = category_item.category_id
		AND category_item.item_id = items.cat_item_id
		AND items.user_id = ? 
		GROUP BY categories.name";
		
		$sql2 = "SELECT items.name FROM categories, category_item, items 
		WHERE items.cat_item_id = category_item.item_id
		AND items.user_id = ?
		AND category_item.category_id = ?
		GROUP BY items.name";
		
		//--------------------------------------------------
		
		$data['user_name'] = $this->user_name;
		$data['categories'] = $this->db->query($sql, array($this->user_id))->result_array();
		
		// if $cat_id null : fallback 
		if($cat_id === null) $cat_id = $data['categories'][0]['id']; // FIX : what if no category at all ?
		$data['current_category'] = $cat_id;
		
		$data['items'] = $this->db->query($sql2, array($this->user_id, $cat_id))->result_array();

		//print_r($data['items']);
		
		$this->load->view('item/view', $data);

	}
	
	public function delete($item_id=null)
	{
		//
	}
	

	

	

	
	

}
?>