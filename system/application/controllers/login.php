<?php

class Login extends Controller {

	function Login()
	{
		parent::Controller();
		$this->load->model('User');
		$this->load->helper('login');
		$params = array('iteration_count_log2' => 8, 'portable_hashes' => FALSE);
		$this->load->library('PasswordHash', $params);
	}
	
	function index($message='')
	{
		$this->load->view('shared/header');
		$this->load->view('login/index', array('message' => $message));
		$this->load->view('shared/footer');
	}

	function signup($message='')
	{
		// TODO checks 
		// password is good enough > 6 
		// has been typed twice identically
		$this->load->view('shared/header', array('message' => $message));
		//----
		if("GET"==$_SERVER['REQUEST_METHOD'])
		{
			$this->load->view('login/signup');
		}
		else if("POST"==$_SERVER['REQUEST_METHOD'])
		{
			try {
				$name = $_POST['name'];
				$email = $_POST['email'];
				$password = $_POST['password'];
				//----
				$new_user = new User($name, $email, $password);
				
				$new_user_id = $new_user->save(); // might throw Exception
				log_message('debug', 'new_user id: ' . $new_user_id);
				$this->log_user($new_user_id, $name);
				redirect('help');
			} 
			catch (Exception $e) {
				$this->load->view('login/signup', array('message' => $e->getMessage()));
			}
		}
		//----
		$this->load->view('shared/footer');
	}

	function check()
	// come here through a POST
	{		
		try {
					$email = $_POST['email'];
					$password = $_POST['password'];
					//$new_user = new User('', $email, $password); 
					$row = User::authenticate($email, $password); // throw Exception if failure
					$this->log_user($row['id'], $row['name']); // set cookie with User ID
					redirect('item');
		}
		catch (Exception $e) {
			$this->index($e->getMessage());
		}
	}
	
	function logout()
	{
		// destroy session
		$this->session->sess_destroy();
		$this->load->view('shared/header');
		$this->load->view('login/index', array('message' => ''));
		$this->load->view('shared/footer');
	}
	
	function log_user($user_id, $user_name)
	{
		$newdata = array('logged_in' => TRUE, 'user_id' => $user_id, 'user_name' => $user_name);
		$this->session->set_userdata($newdata);
	}
}
?>