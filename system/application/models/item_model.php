<?php
class Item_model extends Model {

    private $name;
    private $date;
    private $user_id;

    function Item ($name, $user_id)
    {
        // Call the Model constructor
        parent::Model();
				$this->load->helper('date');
				$this->name = $name;
				$this->user_id = $user_id;
				$this->date = local_to_gmt(time());
    }


    function get_last_ten_entries()
    {
        $query = $this->db->get('entries', 10);
        return $query->result();
    }

    function insert_entry()
    {
        $this->title   = $_POST['title'];
        $this->content = $_POST['content'];
        $this->date    = time();

        $this->db->insert('entries', $this);
    }

    function update_entry()
    {
        $this->title   = $_POST['title'];
        $this->content = $_POST['content'];
        $this->date    = time();

        $this->db->update('entries', $this, array('id', $_POST['id']));
    }

////////////////////////
	protected function filterInputArray($table, $data, $xss_clean = false){
	        $fields = $this->db->list_fields($table);

	        foreach ($data as $k => $v) {
	                if(in_array($k, $fields) == false){
	                        unset($data[$k]);
	                } else {
	                        if($xss_clean === true) $data[$k] = $this->input->xss_clean($v);
	                }
	        }

	        return $data;
	}

	//a typical update method
	public function updateMethod($id, $data = array()){
	        $data = $this->filterInputArray('users', $data);
	        $this->db->where('id', $id);
	        $this->db->update('users', $data);
	        return $this->db->affected_rows();
	}

	//a typical insert method
	public function insertMethod($data = array()){
	        $data = $this->filterInputArray('users', $data);
	        $this->db->insert('users', $data);
	        return $this->db->insert_id();
	}

}
?>
