<? php
class Category extends Model {

    var private $name;
    var private $date;
    var private $item_id;

    function Category ($name)
    {
        // Call the Model constructor
        parent::Model();
		$this->load->helper('date');
		$this->name = $name;
		$this->date = local_to_gmt(time());
    }
}
?>