<?php
class User extends Model {
    private $name   = '';
    private $email = '';
    private $password = '';
    private $token = '';

    function User($name='', $email='', $password='')
    {
        // Call the Model constructor
        parent::Model();
		// Load libraries
		$params = array('iteration_count_log2' => 8, 'portable_hashes' => FALSE);
		$this->load->library('PasswordHash', $params);
		//$this->load->library('Crypt_HMAC', $this->config->item('openomy_private_key'),'sha1');
		// create hashed password
		$t_hasher = new PasswordHash(14, FALSE);
		$hash = $t_hasher->HashPassword($password);
		//log_message('debug', 'hashed passwd : ' . $hash);
		// check validity of $name, $email, $password and throw new Exception('Bad Password')
		// Create user
		$this->name = $name;
		$this->email = $email;
		$this->password = $hash;
		//log_message('user : ', var_dump($this));
		// 
		
    }
		
		function save(){
		// return freshly created user's id
			$sql = "INSERT INTO users (name,email,password) VALUES (?,?,?)";
			$this->db->query($sql, array($this->name, $this->email, $this->password));
			return $this->db->insert_id(); 
		}
		
		
		// Class functions
		
		function authenticate($email, $password) 
		// return a user table row 
		// or throw an exception
		{
			$sql = "SELECT * FROM users WHERE email = ?";
			$query = $this->db->query($sql, array($email));
			if ($query->num_rows() == 1) { // --- login exists !
				$stored_hash = $query->row()->password;
				// Using bcrypt
				$t_hasher = new PasswordHash(14, FALSE);
				$is_correct = $t_hasher->CheckPassword($password, $stored_hash);
				if($is_correct) {
					// create token
					//$this->token = User::create_token();
					$row = $query->row_array();
					return $row;
				} else {
					throw new Exception('Invalid Password');
				}
			} else { 
				throw new Exception('Invalid Email'); 
			}
		}
		
		function create_token(){
			// 'openomy_application_key'
			//
			$date = date('r');
			$relativeUrl = '/2.0/auth/confirmedtokens';
			$url = $this->config->item('openomy_url') . $relativeUrl;
			$pattern = "POST\n$relativeUrl\n$date\n";
			$signature = $this->crypt_hmac->hash($pattern);
			$req = new HttpRequest($url, METH_POST);
			$req->rawPostData = "<user><username>$this->name</username><password>$this->password</password><email>$this->email</email></user>";
			$req.send();
		}
}
?>
