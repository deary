<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr">
 <head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <title></title>
	<!-- <base href="http://127.0.0.1/~manu/Deary/"> -->
	<base href="<?php echo $this->config->item('base_url'); ?>">
	<link rel="stylesheet" href="stylesheets/screen.css" type="text/css" media="screen, projection">
	<link rel="stylesheet" href="stylesheets/deary.css" type="text/css" media="screen, projection">
	<link rel="stylesheet" href="stylesheets/print.css" type="text/css" media="print">    
	<!--[if IE]><link rel="stylesheet" href="ie.css" type="text/css" media="screen, projection"><![endif]-->
	<script type="text/javascript" src="javascripts/jquery.js"></script>
	<script type="text/javascript" src="javascripts/javascrypt.js"></script>
	<script type='text/javascript' src="javascripts/deary.js"></script>

 </head>
 <body>
	<div id="header">
		<!-- <h1>Deary</h1> -->
	</div>