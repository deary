<h3>
	Your Categories
</h3>
<div id="categories">
	<ul>
		<?php foreach($categories as $category):?>
		<li>
			<?php echo $category['name'];?>
		</li>
		<?php endforeach; ?>
	</ul>
	<p>
		<input type="button" value="New Category" onclick="$('#content').load('<?php echo site_url("category/create"); ?>');">
	</p>
</div>
<div id="items">
	<ul>
		<?php foreach($items as $item):?>
		<li>
			<?php echo $item['name'];?>
		</li><?php endforeach; ?>
	</ul>
	<p>
		<input type="button" value="New Item" onclick="$('#content').load('<?php echo site_url("item/create/" . $current_category); ?>');" />
	</p>
</div>

