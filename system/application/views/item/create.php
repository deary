<h2>New item</h2>

<?= form_open('item/save'); ?>
	<p>
		<label for="name">Name</label> <input type="text" name="name" value="" id="name"> 
	</p>
	
<!-- 
$options = array(
                  'small'  => 'Small Shirt',
                  'med'    => 'Medium Shirt',
                  'large'   => 'Large Shirt',
                  'xlarge' => 'Extra Large Shirt',
                );

$shirts_on_sale = array('small', 'large');

echo form_dropdown('shirts', $options, 'large');

-->

	<p>
	<!-- loop on categories table to get the list -->	
		<label for="category">Category</label> 
		<select name="category_ids[]" value="" multiple="multiple" id="category">
			<?php foreach($categories as $category):?>
				<option value="<?php echo $category['id'];?>"><?php echo $category['name'];?></option>
			<?php endforeach;?>
		</select>	</p>
		
			<p>
	<!-- loop on types table to get the list -->	
		<label for="type">Item Type</label> 
		<select name="type" id="type" onchange="getSubItemForm(this);">
			<?php foreach($types as $type):?>
				<option value="<?php echo $type['id'];?>"><?php echo $type['type'];?></option>
			<?php endforeach;?>
		</select>
	</p>
	


	<p><input type="submit" value="Create"></p>
	
</form>

<script type="text/javascript">
function getSubItemForm(selectElt) {
	subItem = selectElt.options[selectElt.selectedIndex].value;
	console.log(subItem);
	jQuery.post( 'item/form', { typeid : subItem }, displayForm);
}
function displayForm(){
	console.log(this);
}
</script>
