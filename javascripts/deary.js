// Deary object
function Deary(){
	
	// dependencies 
	// jQuery library
	// Javascrypt library
	

	
	
	////////////////////////////////////////////////////////////////////
	// Private Members
	////////////////////////////////////////////////////////////////////
	
	//var store = new Persist.Store('Deary'); // local storage for offline use
	var my_key = null; // "a night of bliss with pastor chris";
	var date = new Date();

	////////////////////////////////////////////////////////////////////
	// Private Methods - Adapted from JavaScrypt    
    // For details, see http://www.fourmilab.ch/javascrypt/
	////////////////////////////////////////////////////////////////////
	
	function setTheKey() {
	    var s = encode_utf8(my_key);
	    var i,
	    kmd5e,
	    kmd5o;

	    if (s.length == 1) {
	        s += s;
	    }

	    md5_init();
	    for (i = 0; i < s.length; i += 2) {
	        md5_update(s.charCodeAt(i));
	    }
	    md5_finish();
	    kmd5e = byteArrayToHex(digestBits);

	    md5_init();
	    for (i = 1; i < s.length; i += 2) {
	        md5_update(s.charCodeAt(i));
	    }
	    md5_finish();
	    kmd5o = byteArrayToHex(digestBits);

	    var hs = kmd5e + kmd5o;
	    key = hexToByteArray(hs);
	    hs = byteArrayToHex(key);
	}

	function decryptText(txt) {
	    if (txt.length == 0) {
	        alert("No cipher text to decrypt!  Please enter or paste cipher text in the field above.");
	        return;
	    }
	    setTheKey();
	    var ct = new Array(),
	    kt;
	    kt = determineArmourType(txt);
	    if (kt == 0) {
	        ct = disarm_codegroup(txt);
	    } else if (kt == 1) {
	        ct = disarm_hex(txt);
	    } else if (kt == 2) {
	        ct = disarm_base64(txt);
	    }

	    var result = rijndaelDecrypt(ct, key, "CBC");

	    var header = result.slice(0, 20);
	    result = result.slice(20);

	    /*  Extract the length of the plaintext transmitted and
		    verify its consistency with the length decoded.  Note
		    that in many cases the decrypted messages will include
		    pad bytes added to expand the plaintext to an integral
		    number of AES blocks (blockSizeInBits / 8).  */

	    var dl = (header[16] << 24) | (header[17] << 16) | (header[18] << 8) | header[19];
	    if ((dl < 0) || (dl > result.length)) {
	        //alert("Message (length " + result.length + ") truncated.  " +
	        throw("Message (length " + result.length + ") truncated.  " +
	        dl + " characters expected.");
	        //	Try to sauve qui peut by setting length to entire message
	        dl = result.length;
	    }

	    /*  Compute MD5 signature of message body and verify
		    against signature in message.  While we're at it,
		    we assemble the plaintext result string.  Note that
		    the length is that just extracted above from the
		    message, *not* the full decrypted message text.
		    AES requires all messages to be an integral number
		    of blocks, and the message may have been padded with
		    zero bytes to fill out the last block; using the
		    length from the message header elides them from
		    both the MD5 computation and plaintext result.  */

	    var i,
	    plaintext = "";

	    md5_init();
	    for (i = 0; i < dl; i++) {
	        plaintext += String.fromCharCode(result[i]);
	        md5_update(result[i]);
	    }
	    md5_finish();

	    for (i = 0; i < digestBits.length; i++) {
	        if (digestBits[i] != header[i]) {
	            //alert("Message corrupted.  Checksum of decrypted message does not match.");
				throw "Message corrupted.  Checksum of decrypted message does not match."
	            break;
	        }
	    }

	    //  That's it; plug plaintext into the result field
	    return decode_utf8(plaintext);
	}
	
	function decryptHtml(html_string) {
		var tag = false;
		var c, buffer = "", final_string = "";
		for(var i=0; i<html_string.length; i++){
			c = html_string.charAt(i);
			if(c === "<") {
				if(buffer !== "") {
					try {
						final_string += decryptText(buffer);
					} catch(e){
						final_string += buffer;
					}
					buffer = "";
				}
				tag = true;
				final_string += c;
			
			} else if(c === ">") {
				
				tag = false;
				final_string += c;
			
			} else {
			
				if(tag === true) 
					final_string += c;
				else buffer += c;
			
			}
		}
		return final_string;
	}

	function encryptText(txt) {
	    var v, i, encrypted_text;

	    if (my_key.length == 0) {
	        alert("Please specify a key with which to encrypt the message.");
	        return;
	    }
	    if (txt.length == 0) {
	        alert("No plain text to encrypt!  Please enter or paste plain text in the field above.");
	        return;
	    }
	    encrypted_text = "";
	    setTheKey();

	    addEntropyTime();
	    prng = new AESprng(keyFromEntropy());
	    var plaintext = encode_utf8(txt);

	    //  Compute MD5 sum of message text and add to header
	    md5_init();
	    for (i = 0; i < plaintext.length; i++) {
	        md5_update(plaintext.charCodeAt(i));
	    }
	    md5_finish();
	    var header = "";
	    for (i = 0; i < digestBits.length; i++) {
	        header += String.fromCharCode(digestBits[i]);
	    }

	    //  Add message length in bytes to header
	    i = plaintext.length;
	    header += String.fromCharCode(i >>> 24);
	    header += String.fromCharCode(i >>> 16);
	    header += String.fromCharCode(i >>> 8);
	    header += String.fromCharCode(i & 0xFF);

	    /*  The format of the actual message passed to rijndaelEncrypt
		    is:

		    	    Bytes   	Content
			     0-15   	MD5 signature of plaintext
			    16-19   	Length of plaintext, big-endian order
			    20-end  	Plaintext

		    Note that this message will be padded with zero bytes
		    to an integral number of AES blocks (blockSizeInBits / 8).
		    This does not include the initial vector for CBC
		    encryption, which is added internally by rijndaelEncrypt.

		*/

	    var ct = rijndaelEncrypt(header + plaintext, key, "CBC");
	    if (document.plain.encoding[0].checked) {
	        v = armour_codegroup(ct);
	    } else if (document.plain.encoding[1].checked) {
	        v = armour_hex(ct);
	    } else if (document.plain.encoding[2].checked) {
	        v = armour_base64(ct);
	    }
	    encrypted_text = v;
	    delete prng;
	}
	
	////////////////////////////////////////////////////////////////////
	// Privileged (Public) Methods
	////////////////////////////////////////////////////////////////////

	this.loadDocument = function(url, target) {
		$.get(url, function(data){
			var clear_html = decryptHtml(data);
			$(target).html(clear_html);
		});
	}
	
	this.recordKey = function(key, url, target) {
		my_key = key;
		deary.loadDocument(url, target);
	}

	this.countDown = function(){
		var new_date = new Date();
		//console.log(new_date.getTime() - date.getTime());
		if(new_date.getTime() - date.getTime() > 10 * 60 * 1000) window.location.reload(false);
		window.setTimeout( "deary.countDown()", 3 * 1000 );
	}
	
	this.resetCountDown = function(){
		date = new Date();
		//console.log("date = %s", date);
	}
	
	/////////////////////////////////////////////////////////////////////
	
	this.countDown();
}


// Initialization 
var deary = new Deary();
$(document).ready(function(){
	$(document).bind("click keypress", function(e){
		deary.resetCountDown();
	});
});



//console.log("deary.js loaded")